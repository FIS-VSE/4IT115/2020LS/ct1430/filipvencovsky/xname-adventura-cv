package cz.vse.java.filipvencovsky.adventuracviceni.logika;


import cz.vse.java.filipvencovsky.adventuracviceni.main.Pozorovatel;
import cz.vse.java.filipvencovsky.adventuracviceni.main.Pozorovatelny;

import java.util.HashSet;
import java.util.Set;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Alena Buchalcevova
 *@version    z kurzu 4IT101 pro školní rok 2014/2015
 */
public class HerniPlan implements Pozorovatelny {
    
    private Prostor aktualniProstor;
    private Prostor viteznyProstor;

    //seznam pozorovatelů změn
    //Set protože nezáleží na pořadí a chceme každého pozorovatele jen jednou
    private Set<Pozorovatel> seznamPozorovatelu = new HashSet<>();
    
     /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor domecek = new Prostor("domeček","domeček, ve kterém bydlí Karkulka");
        Prostor chaloupka = new Prostor("chaloupka", "chaloupka, ve které bydlí babička Karkulky");
        Prostor jeskyne = new Prostor("jeskyně","stará plesnivá jeskyně");
        Prostor les = new Prostor("les","les s jahodami, malinami a pramenem vody");
        Prostor hlubokyLes = new Prostor("hluboký_les","temný les, ve kterém lze potkat vlka");
        
        // přiřazují se průchody mezi prostory (sousedící prostory)
        domecek.setVychod(les);
        les.setVychod(domecek);
        les.setVychod(hlubokyLes);
        hlubokyLes.setVychod(les);
        hlubokyLes.setVychod(jeskyne);
        hlubokyLes.setVychod(chaloupka);
        jeskyne.setVychod(hlubokyLes);
        chaloupka.setVychod(hlubokyLes);
                
        aktualniProstor = domecek;  // hra začíná v domečku  
        viteznyProstor = chaloupka ;
        les.vlozVec(new Vec("maliny", true));
        les.vlozVec(new Vec("strom", false));  
    }
    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
       //v tomto momentu se změní model a hráč se ocitne v jiném prostoru
       // je to prní možné místo pro zaznamenání změny
       // nejlepší místo pro pozorování změny prostoru je u třídy, která vlastní atribut aktuální prostor
       upozorniPozorovatele();
    }
    /**
     *  Metoda vrací odkaz na vítězný prostor.
     *
     *@return     vítězný prostor
     */
    
    public Prostor getViteznyProstor() {
        return viteznyProstor;
    }

    /**
     * Metoda slouží pro registraci pozorovatelů,
     * kteří mají zájem dozvědět se o změnách
     *
     * @param pozorovatel
     */
    @Override
    public void registrace(Pozorovatel pozorovatel) {
        //nejjednodušší možná implementace seznamu pozorovatelů
        seznamPozorovatelu.add(pozorovatel);
    }

    /**
     * Metoda pro odebrání pozorovatele ze seznamu
     * pozorovatelů, kterým jsou zasílány změny
     *
     * @param pozorovatel
     */
    @Override
    public void odregistrace(Pozorovatel pozorovatel) {
        seznamPozorovatelu.remove(pozorovatel);
    }

    /**
     * Metoda upozorní všechny registrované pozorovatele
     * na změnu tím, že zavolá jejich metodu update
     */
    @Override
    public void upozorniPozorovatele() {
        //chceme volat metodu update každého pozorovatele
        for(Pozorovatel pozorovatel : seznamPozorovatelu) {
            pozorovatel.update();
        }

    }
}
