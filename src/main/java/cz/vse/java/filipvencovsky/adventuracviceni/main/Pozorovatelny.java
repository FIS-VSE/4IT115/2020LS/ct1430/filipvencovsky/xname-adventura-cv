package cz.vse.java.filipvencovsky.adventuracviceni.main;

/**
 * Rozhraní pro pozorovatelné objekty.
 * Pozorovatelný objekt může odesílat změny svým pozorovatelům
 */
public interface Pozorovatelny {

    /**
     * Metoda slouží pro registraci pozorovatelů,
     * kteří mají zájem dozvědět se o změnách
     * @param pozorovatel
     */
    void registrace(Pozorovatel pozorovatel);

    /**
     * Metoda pro odebrání pozorovatele ze seznamu
     * pozorovatelů, kterým jsou zasílány změny
     * @param pozorovatel
     */
    void odregistrace(Pozorovatel pozorovatel);

    /**
     * Metoda upozorní všechny registrované pozorovatele
     * na změnu tím, že zavolá jejich metodu update
     */
    void upozorniPozorovatele();

}
