package cz.vse.java.filipvencovsky.adventuracviceni.main;

/**
 * Rozhraní pozorovatel umožňuje příjem změn
 * od pozorovatelných objektů
 */
public interface Pozorovatel {

    /**
     * Metoda, kterou volá pozorovatelný objekt
     * při nastalé změně. V těle metody může pozorovatel
     * na změnu zareagovat.
     */
    void update();
}
