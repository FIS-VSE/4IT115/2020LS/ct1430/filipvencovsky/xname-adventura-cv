package cz.vse.java.filipvencovsky.adventuracviceni.main;

import cz.vse.java.filipvencovsky.adventuracviceni.logika.Hra;
import cz.vse.java.filipvencovsky.adventuracviceni.logika.IHra;
import cz.vse.java.filipvencovsky.adventuracviceni.logika.Prostor;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
//raději zkontrolujeme import třídy Point2D, že je z balíčku javafx
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

import java.util.HashMap;
import java.util.Map;

/**
 * Controller chce přijímat změny modelu, bude proto implemetnovat vzor pozorovatel
 */
public class MainController implements Pozorovatel {

    @FXML
    public TextField vstup;
    @FXML
    public TextArea vystup;
    @FXML
    public Button odesli;
    @FXML
    public ListView vychody;
    @FXML
    public ImageView hrac;

    private IHra hra;

    //mapa souřadnic
    //pro souřadnice použijeme přepravku z javafx Point2D
    private Map<String, Point2D> souradniceProstoru = new HashMap<>();

    /**
     * kontstruktor bez parametrů bude sloužit k vytvoření hry
     */
    public MainController() {
        this.hra = new Hra();

        //není vhodné souřadnice ukládat do byznys tříd, protože závisí na konkrétní velikosti mapy
        //ideální by bylo použít konfigurační soubor, např. json nebo yaml
        //zatím bude stačit vytvořit mapu s klíčem jako názvem prostoru a hodnotou souřadnic
        //souřadnice získáme polohováním hráče v scenebuilderu
        //nezapomeneme pak přesunout hráče na pozici 0,0, layoutX a X se totiž sčítá
        souradniceProstoru.put("domeček",new Point2D(14,96));
        souradniceProstoru.put("chaloupka",new Point2D(201,59));
        souradniceProstoru.put("jeskyně",new Point2D(170,166));
        souradniceProstoru.put("les",new Point2D(79, 61));
        souradniceProstoru.put("hluboký_les",new Point2D(142,98));
    }

    /**
     * metoda initialize se injektuje z FXML
     * spouští se po načtení FXML, spolu se všemi komponentami
     * volá se logicky až po konstruktoru
     */
    @FXML
    public void initialize() {

        //zamezení vstupu do pole
        vystup.setEditable(false);

        //vrácení uvítání a zapsání do výstupního pole
        vystup.appendText(hra.vratUvitani()+"\n\n");

        //chceme pozorovat změny (např. prostoru), které posílá herní plán
        //je potřeba se do herního plánu zaregistrovat jako pozorovatel
        // registrujeme sebe
        hra.getHerniPlan().registrace(this);

        //zavoláme metodu pro aktualizaci změn, která všechny prvky zobrazí v aktuálním stavu
        update();
    }

    /**
     * metoda načte vstup z TextFieldu a vypíše do výstupního pole TextArea
     * @param actionEvent
     */
    public void zpracujVstup(ActionEvent actionEvent) {
        //načtení vstupu z komponenty TextField
        String prikaz = vstup.getText();

        //po vypsání je potřeba smazat obsah pole
        vstup.setText("");

        odesliPrikaz(prikaz);
    }

    private void odesliPrikaz(String prikaz) {
        //namísto do konzole vypíšeme text do TextArea
        //appendText připisuje nový text ke stávajícímu
        //\n slouží pro zalomení řádku
        vystup.appendText(prikaz+"\n");

        //na tomto místě budeme zpracovávat příkazy ze vstupu hrou
        //hru ale musíme ke kontroleru připojit (viz https://stackoverflow.com/questions/34785417/javafx-fxml-controller-constructor-vs-initialize-method)
        String vysledek = hra.zpracujPrikaz(prikaz);

        //výsledek příkazu zapíšeme do výstupního pole
        vystup.appendText(vysledek+"\n\n");

        //zatím nemáme vhodnější místo pro vyhodnocení, že hra skončila než po odeslání příkazu
        if(hra.konecHry()) {
            //po konci hry vrátíme epilog a zamezíme dalšímu vstupu
            vystup.appendText(hra.vratEpilog());

            //zamezení zadávání příkazů
            vstup.setDisable(true);
            odesli.setDisable(true);
            //po konci hry musíme zamezit i klikání na seznam východů
            vychody.setDisable(true);
        }
    }

    /**
     * Metoda, kterou volá pozorovatelný objekt
     * při nastalé změně. V těle metody může pozorovatel
     * na změnu zareagovat.
     */
    @Override
    public void update() {
        System.out.println("Pozorovatelný objekt zasílá změnu");

        //smazání neaktuálního výpisu východů
        vychody.getItems().clear();

        //lokální proměnná prostor pro kratší zápisy
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();

        //naplnění senzamu východů
        //naplnění se provede přes model komponenty, pomocí getItems
        //můžeme přidat všechny prostory najednou
        //v seznamu prostorů je class path a hash, je to způsobeno defaultní implementací metody toString
        //musíme najít třídu prostor a přepsat metodu toString, aby reprezentovala název prostoru
        vychody.getItems().addAll(aktualniProstor.getVychody());

        //kromě seznamu východů musíme aktualizovat i polohu hráče na mapě
        //nejdříve si získáme odkaz na hráče
        //souřadnice se nastavují pomocí metod setX a setY
        //aby bylo možné nastavit hráče na pozici, musíme znát souřadnice všech prostorů
        //teď už jen stačí vrátit přes mapu správné souřadnice
        hrac.setX(souradniceProstoru.get(aktualniProstor.getNazev()).getX());
        hrac.setY(souradniceProstoru.get(aktualniProstor.getNazev()).getY());
    }

    /**
     * Metoda přečte aktuální Prostor, který vybral hráč
     * a předá instrukci o přesunu Hře
     * @param mouseEvent
     */
    public void nactiVychod(MouseEvent mouseEvent) {
        // Načtení vybrané položky ListView se provede pomocí SelectionModelu
        // getSelectedItem vrací prvek ze seznamu - Prostor
        Prostor cil = (Prostor) vychody.getSelectionModel().getSelectedItem();

        //pro kontrolu
        System.out.println(cil);

        // hře můžeme předat příkaz složený ze zvoleného prostoru
        // Také je možné změnit prostor přímo v hermín plánu pomocí setAktualniProstor,
        // to ale není vhodné, protože nemáme jistotu, jestli příkaz jdi neobsahuje
        // nebo nebude obsahovat i nějaký další kód.
        // Problém je, že výstup nereflektuje, co se stalo.
        // Většina toho, co potřebujeme napsat, je v metodě zpracujVstup
        // není ale vhodné mít stejný kód na dvou místech
        // provedeme proto refaktoring
        odesliPrikaz("jdi "+cil);
    }
}
